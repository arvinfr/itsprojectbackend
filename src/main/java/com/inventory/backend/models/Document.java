package com.inventory.backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToMany;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    @ManyToMany
    private Set<Product> products;
    private DocumentType documentType;
    private Date created_at;
    private Date updated_at;

    public Document() {
        this.created_at = Calendar.getInstance().getTime();
        this.updated_at = Calendar.getInstance().getTime();
    }

    public Document(String description, Set<Product> products, DocumentType documentType) {
        this.description = description;
        this.products = products;
        this.documentType = documentType;
        this.created_at = Calendar.getInstance().getTime();
        this.updated_at = Calendar.getInstance().getTime();
    }

    public long getId() {
        return id;
    }

    @lombok.Generated
    public String getDescription() {
        return description;
    }

    @lombok.Generated
    public void setDescription(String description) {
        this.description = description;
    }


    @JsonManagedReference
    @lombok.Generated
    public Set<Product> getProducts() {
        return products;
    }

    @lombok.Generated
    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @lombok.Generated
    public DocumentType getDocumentType() {
        return documentType;
    }

    @lombok.Generated
    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    @lombok.Generated
    public Date getCreated_at() {
        return created_at;
    }

    @lombok.Generated
    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    @lombok.Generated
    public Date getUpdated_at() {
        return updated_at;
    }

    @lombok.Generated
    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public boolean addProduct(Product product) {
        List<Product> foundProducts = this.products.stream().filter(item -> item.getName().equals(product.getName()))
                .collect(Collectors.toList());
        if (foundProducts.size() == 1)
            product.setQuantity(foundProducts.get(0).getQuantity() + product.getQuantity());
        return this.products.add(product);

    }

    public boolean removeProduct(String product_name) {
        Optional<Product> value = this.products.stream().filter(item -> item.getName().equals(product_name)).findAny();
        if(value.isPresent()){
            return this.products.remove(value.get());
        }
        return false;

    }

    @Override
    public String toString() {
        return "Document [description=" + description + ", documentType=" + documentType + ", products=" + products
                + "]";
    }


}
