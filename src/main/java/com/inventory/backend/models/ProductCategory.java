package com.inventory.backend.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class ProductCategory {
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", fetch = FetchType.LAZY)
    private Set<Product> products;
    private String name;
    private Date created_at;
    private Date updated_at;

    public ProductCategory() {
    }

    public ProductCategory(String name) {
        this.name = name;
        this.products = new HashSet<Product>();
    }

    @lombok.Generated
    public void setId(Long id) {
        this.id = id;
    }

    @lombok.Generated
    public Long getId() {
        return id;
    }
    
    @JsonManagedReference
    public Set<Product> getProducts() {
        return products;
    }

    @lombok.Generated
    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @lombok.Generated
    public String getName() {
        return name;
    }

    @lombok.Generated
    public void setName(String name) {
        this.name = name;
    }

    @lombok.Generated
    public Date getCreated_at() {
        return created_at;
    }

    @lombok.Generated
    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    @lombok.Generated
    public Date getUpdated_at() {
        return updated_at;
    }

    @lombok.Generated
    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "ProductCategory [created_at=" + created_at + ", id=" + id + ", name=" + name
                + ", updated_at=" + updated_at + "]";
    }



    
}
