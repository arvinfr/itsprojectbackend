package com.inventory.backend.models;

public class AuthenticationRequest {

    private String userName;
    private String password;

    @lombok.Generated
    public AuthenticationRequest() {
    }

    @lombok.Generated
    public AuthenticationRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    @lombok.Generated
    public String getUserName() {
        return userName;
    }

    @lombok.Generated
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @lombok.Generated
    public String getPassword() {
        return password;
    }

    @lombok.Generated
    public void setPassword(String password) {
        this.password = password;
    }

}
