package com.inventory.backend.models;

public enum DocumentType {
    ImportDocument,
    ExportDocument
}
