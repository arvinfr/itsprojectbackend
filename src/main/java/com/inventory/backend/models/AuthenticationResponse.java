package com.inventory.backend.models;

public class AuthenticationResponse{
    
    private final String jwt;

    @lombok.Generated
    public AuthenticationResponse(String jwt) {
        this.jwt = jwt;
    }

    @lombok.Generated
    public String getJwt() {
        return jwt;
    }

    
}
