package com.inventory.backend.models;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.processing.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class User{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String userName;
  private String password;
  private boolean active;
  private String roles;
  private Date created_at;
  private Date updated_at;

  public User(String userName, String password) {
    this.userName = userName;
    this.password = password;
    this.active = true;
    this.roles = "ADMIN";
    this.created_at = Calendar.getInstance().getTime();
    this.updated_at = Calendar.getInstance().getTime();
  }

  public User() {
  }


  public Long getId() {
    return id;
  }


  public String getUserName() {
    return userName;
  }

  @lombok.Generated
  public void setUserName(String userName) {
    this.userName = userName;
  }

  @lombok.Generated
  public String getPassword() {
    return password;
  }

  @lombok.Generated
  public void setPassword(String password) {
    this.password = password;
  }

  @lombok.Generated
  public Date getCreated_at() {
    return created_at;
  }

  @lombok.Generated
  public Date getUpdated_at() {
    return updated_at;
  }

  @lombok.Generated
  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }

  @Override
  public String toString() {
    return "User [created_at=" + created_at + ", id=" + id + ", password=" + password + ", updated_at=" + updated_at
        + ", userName=" + userName + "]";
  }

  @lombok.Generated
  public boolean isActive() {
    return active;
  }

  @lombok.Generated
  public void setActive(boolean active) {
    this.active = active;
  }

  @lombok.Generated
  public String getRoles() {
    return roles;
  }

  @lombok.Generated
  public void setRoles(String roles) {
    this.roles = roles;
  }
  
}
