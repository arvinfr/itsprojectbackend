package com.inventory.backend.models;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.annotation.processing.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class Product {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @PrimaryKeyJoinColumn(name="category_id")
    private ProductCategory category;
    @ManyToMany(mappedBy = "products")
    private Set<Document> documents;
    private String name;

    private String description;

    private double quantity;
    private String location;
    private double bought_price;
    private double sell_price;
    private Date created_at;
    private Date updated_at;

    public Product() {
        this.created_at = Calendar.getInstance().getTime();
        this.updated_at = Calendar.getInstance().getTime();
    }

    public Product(ProductCategory category, String name) {
        this.category = category;
        this.name = name;
        this.created_at = Calendar.getInstance().getTime();
        this.updated_at = Calendar.getInstance().getTime();
    }

    @lombok.Generated
    public void setId(Long id) {
        this.id = id;
    }

    @lombok.Generated
    public Long getId() {
        return id;
    }

    @JsonBackReference
    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
        this.updated_at = Calendar.getInstance().getTime();
    }

    @lombok.Generated
    public String getName() {
        return name;
    }

    @lombok.Generated
    public void setName(String name) {
        this.name = name;
    }

    @lombok.Generated
    public String getDescription() {
        return description;
    }

    @lombok.Generated
    public void setDescription(String description) {
        this.description = description;
    }

    @lombok.Generated
    public double getQuantity() {
        return quantity;
    }

    @lombok.Generated
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @lombok.Generated
    public String getLocation() {
        return location;
    }

    @lombok.Generated
    public void setLocation(String location) {
        this.location = location;
    }

    @lombok.Generated
    public double getBought_price() {
        return bought_price;
    }

    @lombok.Generated
    public void setBought_price(double bought_price) {
        this.bought_price = bought_price;
    }

    @lombok.Generated
    public double getSell_price() {
        return sell_price;
    }

    @lombok.Generated
    public void setSell_price(double sell_price) {
        this.sell_price = sell_price;
    }

    @lombok.Generated
    public Date getCreated_at() {
        return created_at;
    }

    @lombok.Generated
    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    @lombok.Generated
    public Date getUpdated_at() {
        return updated_at;
    }

    @lombok.Generated
    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    @lombok.Generated
    @JsonIgnore
    public Set<Document> getDocuments() {
        return documents;
    }

    @lombok.Generated
    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "Product [bought_price=" + bought_price + ", category=" + category.toString() + ", created_at=" + created_at
                + ", description=" + description + ", id=" + id + ", location=" + location + ", name=" + name
                + ", quantity=" + quantity + ", sell_price=" + sell_price + ", updated_at=" + updated_at + "]";
    }


}
