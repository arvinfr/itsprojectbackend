package com.inventory.backend.controllers;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.inventory.backend.DTO.ProductCategoryDTO;
import com.inventory.backend.DTO.ProductDTO;
import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;
import com.inventory.backend.repositories.ProductCategoryRepository;
import com.inventory.backend.repositories.ProductRepository;

import com.inventory.backend.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//@AllArgsConstructor
@RestController
public class ProductController {

    @Autowired
    ProductService service;
    @Autowired
    ProductCategoryRepository categoryRepository;

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/categories")
//    @RequestMapping(value = "/categories", method = {RequestMethod.GET,RequestMethod.OPTIONS})
    public List<ProductCategory> listCategories() {
        return (List<ProductCategory>) categoryRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/categories/{id}")
    public ProductCategory categoryById(@PathVariable Long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new ProductCategoryNotFoundExeption(id));
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/categories")
    public ProductCategory newCategory(@RequestBody ProductCategoryDTO newCategoryDTO) {
        return service.addCategory(newCategoryDTO);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/categories/{id}")
    public ProductCategory updateCategory(@RequestBody ProductCategoryDTO newCategoryDTO, @PathVariable Long id) {
        newCategoryDTO.Id = id;
        return service.updateCategory(newCategoryDTO);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @DeleteMapping("/categories/{id}")
    public void deleteProductCategory(@PathVariable Long id) {
        categoryRepository.deleteById(id);
    }

    @Autowired
    ProductRepository repository;

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/products")
    public List<Product> listProducts() {
        return (List<Product>) repository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/products")
    public Product newProduct(@RequestBody ProductDTO newProductDTO) {
        return service.addProduct(newProductDTO);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/products/{id}")
    public Product productById(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/categories/{category_id}/products")

    public List<Product> listProductsByCategory(@PathVariable Long category_id) {
        Optional<ProductCategory> value = categoryRepository.findById(category_id);
        if (value.isPresent()) {
            return (List<Product>) repository.findAllByCategory(value.get());
        }
        return null;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @PutMapping("/products/{id}")
    public Product updateProduct(@RequestBody ProductDTO newProductDTO, @PathVariable Long id) {
        newProductDTO.id = id;
        return service.updateProduct(newProductDTO);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
