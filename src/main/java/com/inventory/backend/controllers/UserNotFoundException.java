package com.inventory.backend.controllers;

public class UserNotFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 1L;

    public UserNotFoundException(Long id) {
        super("Could not find User " + id);
      }
    public UserNotFoundException(String userName) {
        super("Could not find User " + userName);
    }
}
