package com.inventory.backend.controllers;

import java.util.List;

import com.inventory.backend.DTO.UserDTO;
import com.inventory.backend.models.User;
import com.inventory.backend.repositories.UserRepository;
import com.inventory.backend.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin("http://localhost:8080/**")
public class UserController {

    @Autowired
    UserRepository repository;
    @Autowired
    UserService service;

    @GetMapping("/users")
    public List<User> list(){
        return (List<User>) repository.findAll();
    }

    @PostMapping("/users/register")
    public User newUser(@RequestBody UserDTO newUserDTO){
        return service.registerNewUserAccount(newUserDTO);
    }

        
    @GetMapping("/users/{id}")
    public User GetById(@PathVariable Long id) {

        return repository.findById(id)
        .orElseThrow(() -> new UserNotFoundException(id));
    }

    @GetMapping("/login/{userName}")
    public User Login(@PathVariable String userName) {
        return repository.findByUserName(userName)
                .orElseThrow(() -> new UserNotFoundException(userName));

    }


    @PutMapping("/users/{id}")
    public User updateUser(@RequestBody UserDTO newUserDTO, @PathVariable Long id) {
        return service.updateUserDetails(newUserDTO, id);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
