package com.inventory.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ProductCategoryNotFoundExeption extends RuntimeException{
    
    private static final long serialVersionUID = 1L;

    public ProductCategoryNotFoundExeption(Long id) {
        super("Could not find Product Category " + id);
      }    
}
