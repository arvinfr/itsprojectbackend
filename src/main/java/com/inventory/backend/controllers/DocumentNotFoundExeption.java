package com.inventory.backend.controllers;

public class DocumentNotFoundExeption extends RuntimeException {
    private static final long serialVersionUID = 1L;

    DocumentNotFoundExeption(Long id) {
        super("Could not find Document " + id);
    }

}
