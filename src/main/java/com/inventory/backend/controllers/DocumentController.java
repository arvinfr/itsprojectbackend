package com.inventory.backend.controllers;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.inventory.backend.DTO.DocumentDTO;
import com.inventory.backend.DTO.ProductDTO;
import com.inventory.backend.models.Document;
import com.inventory.backend.models.DocumentType;
import com.inventory.backend.models.Product;
import com.inventory.backend.repositories.DocumentRepository;

import com.inventory.backend.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:8080")
public class DocumentController {

    @Autowired
    DocumentRepository repository;

    @Autowired
    ProductRepository productRepository;

    @GetMapping("/documents")
    public List<Document> listDocuments() {
        return (List<Document>) repository.findAll();
    }

    @PostMapping("/documents")
    public Document newDocument(@RequestBody DocumentDTO newDocumentDTO) {

        if(newDocumentDTO.documentType != null){
            Document document = new Document();
            document.setDescription(newDocumentDTO.description);
            document.setProducts(newDocumentDTO.products);
            List<Product> productList = newDocumentDTO.products.stream().collect(Collectors.toList());
            for(int i=0;i<productList.size();i++){

                Product changedProduct = productRepository.findById(productList.get(i).getId()).get();
                if(newDocumentDTO.documentType == DocumentType.ExportDocument) {
                    changedProduct.setQuantity(changedProduct.getQuantity() - productList.get(i).getQuantity());
                }
                else {
                    changedProduct.setQuantity(changedProduct.getQuantity() + productList.get(i).getQuantity());
                }
                productRepository.save(changedProduct);
            }
            document.setDocumentType(newDocumentDTO.documentType);
            return repository.save(document);
        }
        return null;
    }

    @GetMapping("/documents/type/{type}")
    public List<Document> listDocumentsByType(@PathVariable DocumentType type){
        return (List<Document>) repository.findAllByDocumentType(type);
    }

    @GetMapping("/documents/{id}")
    public Document documentById(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new DocumentNotFoundExeption(id));
    }



    @PutMapping("/documents/{id}")
    public Document updateDocument(@RequestBody DocumentDTO newDocumentDTO, @PathVariable Long id) {

        return repository.findById(id).map(Document -> {

            if (newDocumentDTO.description == null || newDocumentDTO.description.isBlank()) {
                Document.setDescription(documentById(id).getDescription());
            }
            if (newDocumentDTO.products == null) {
                Document.setProducts(documentById(id).getProducts());
            }
            return repository.save(Document);

        }).orElseThrow(() -> new DocumentNotFoundExeption(newDocumentDTO.id));
    }

    @DeleteMapping("/documents/{id}")
    public void deleteDocument(@PathVariable Long id) {
        repository.deleteById(id);
    }

}
