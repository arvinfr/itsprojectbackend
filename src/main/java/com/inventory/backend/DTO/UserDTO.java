package com.inventory.backend.DTO;

import com.inventory.backend.models.User;

public class UserDTO {
    
    private Long Id;
    private String userName;
    private String password;

    public Long getId() {
        return Id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User createUser(){
        return new User(this.userName, this.password);
    }
    

}
