package com.inventory.backend.DTO;

import com.inventory.backend.models.ProductCategory;

public class ProductDTO {
    public Long id;
    public ProductCategory category;
    public String name;
    public String description;
    public double quantity;
    public String location;
    public double bought_price;
    public double sell_price;

    public ProductDTO(){

    }
}
