package com.inventory.backend.DTO;

import com.inventory.backend.models.Product;
import org.springframework.stereotype.Component;

import java.util.Set;

public class ProductCategoryDTO {
    public Long Id;
    public Set<Product> products;
    public String name;
}
