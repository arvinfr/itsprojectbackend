package com.inventory.backend.DTO;

import com.inventory.backend.models.DocumentType;
import com.inventory.backend.models.Product;

import java.util.Set;

public class DocumentDTO {
    public Long id;
    public String description;
    public Set<Product> products;
    public DocumentType documentType;

    public DocumentDTO(){

    }
}
