package com.inventory.backend.services;

import com.inventory.backend.DTO.ProductCategoryDTO;
import com.inventory.backend.DTO.ProductDTO;
import com.inventory.backend.controllers.ProductCategoryNotFoundExeption;
import com.inventory.backend.controllers.ProductNotFoundException;
import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;
import com.inventory.backend.repositories.ProductCategoryRepository;
import com.inventory.backend.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class ProductService {

    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Autowired
    ProductRepository productRepository;

    public ProductCategory addCategory(ProductCategoryDTO productCategoryDTO) {
        ProductCategory productCategory = null;
        if (productCategoryDTO.name != null && !productCategoryDTO.name.isBlank()) {
            productCategory = new ProductCategory(productCategoryDTO.name);
            productCategory.setCreated_at(Calendar.getInstance().getTime());
            productCategory.setUpdated_at(Calendar.getInstance().getTime());
            productCategoryRepository.save(productCategory);
        }
        return productCategory;
    }

    public ProductCategory updateCategory(ProductCategoryDTO productCategoryDTO) {
        return productCategoryRepository.findById(productCategoryDTO.Id).map(category -> {
            category.setName(productCategoryDTO.name);
            category.setUpdated_at(Calendar.getInstance().getTime());
            return productCategoryRepository.save(category);
        }).orElseThrow(() -> new ProductCategoryNotFoundExeption(productCategoryDTO.Id));
    }


    public Product addProduct(ProductDTO productDTO) {

        if(!productCategoryRepository.findById(productDTO.category.getId()).isPresent()){
            throw new ProductCategoryNotFoundExeption(productDTO.category.getId());
        }

        Product product = null;
        if (productDTO.name != null && !productDTO.name.isBlank() && productDTO.category != null) {
            product = new Product(productDTO.category,productDTO.name);
            product.setBought_price(productDTO.bought_price);
            product.setDescription(productDTO.description);
            product.setSell_price(productDTO.sell_price);
            product.setLocation(productDTO.location);
            product.setQuantity(productDTO.quantity);
            product.setCreated_at(Calendar.getInstance().getTime());
            product.setUpdated_at(Calendar.getInstance().getTime());
            productRepository.save(product);
        }
        return product;
    }

    public Product updateProduct(ProductDTO productDTO){

        return productRepository.findById(productDTO.id).map(Product -> {
            if(!Double.isNaN(productDTO.bought_price)){
                Product.setBought_price(productDTO.bought_price);
            }
            if(!Double.isNaN(productDTO.sell_price)){
                Product.setSell_price(productDTO.sell_price);
            }
            if(!Double.isNaN(productDTO.quantity)){
                Product.setQuantity(productDTO.quantity);
            }
            if(productDTO.name != null && !productDTO.name.isBlank()){
                Product.setName(productDTO.name);
            }
            if(productDTO.location != null && !productDTO.location.isBlank()){
                Product.setLocation(productDTO.location);
            }
            Product.setUpdated_at(Calendar.getInstance().getTime());
            return productRepository.save(Product);
        }).orElseThrow(() -> new ProductNotFoundException(productDTO.id));
    }



}
