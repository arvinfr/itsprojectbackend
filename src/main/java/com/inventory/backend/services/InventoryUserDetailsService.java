package com.inventory.backend.services;

import com.inventory.backend.models.InventoryUserDetails;
import com.inventory.backend.models.User;
import com.inventory.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InventoryUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return userRepository.findByUserName(userName).map(InventoryUserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
    }


}
