package com.inventory.backend.services;

import java.util.Calendar;

import com.inventory.backend.DTO.UserDTO;
import com.inventory.backend.controllers.UserNotFoundException;
import com.inventory.backend.models.User;
import com.inventory.backend.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    public User registerNewUserAccount(UserDTO userDTO) {
        User user = userDTO.createUser();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User updateUserDetails(UserDTO userDTO, Long Id) throws UserNotFoundException {
        return userRepository.findById(Id).map(User -> {
            if (userDTO.getPassword() != null && !userDTO.getPassword().isBlank()) {
                User.setPassword(passwordEncoder.encode(userDTO.getPassword()));
                User.setUpdated_at(Calendar.getInstance().getTime());
            }
            return userRepository.save(User);
        }).orElseThrow(() -> new UserNotFoundException(Id));
    }
}
