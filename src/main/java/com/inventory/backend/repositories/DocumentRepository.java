package com.inventory.backend.repositories;

import com.inventory.backend.models.Document;
import com.inventory.backend.models.DocumentType;

import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<Document,Long> {
    Iterable<Document> findAllByDocumentType(DocumentType type);
}
