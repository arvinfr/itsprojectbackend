package com.inventory.backend.repositories;

import java.util.Optional;

import com.inventory.backend.models.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUserName(String userName);
}
