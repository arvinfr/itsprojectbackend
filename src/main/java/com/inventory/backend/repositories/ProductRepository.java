package com.inventory.backend.repositories;

import java.util.List;

import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {

    Product findProductByName(String name);
    List<Product> findAllByCategory(ProductCategory category);
}
