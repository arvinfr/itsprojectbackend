package com.inventory.backend.repositories;

import com.inventory.backend.models.ProductCategory;

import org.springframework.data.repository.CrudRepository;

public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Long> {

    ProductCategory findProductCategoryByName(String name);

}
