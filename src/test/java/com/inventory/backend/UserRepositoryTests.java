package com.inventory.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.inventory.backend.models.User;
import com.inventory.backend.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void init() {
        userRepository.deleteAll();
    }

    @Test
    void saveUser() {
        User user1 = new User("user1", "pass1");
        userRepository.save(user1);
        assertEquals(user1.toString(), userRepository.findById(user1.getId()).get().toString());
    }

    @Test
    void findUserFromUserName() {
        User user1 = new User("user1", "pass1");
        userRepository.save(user1);
        User retrieved_user = userRepository.findByUserName(user1.getUserName()).get();
        assertEquals(user1.toString(), retrieved_user.toString());
    }

}
