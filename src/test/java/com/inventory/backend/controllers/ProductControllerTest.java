package com.inventory.backend.controllers;

import com.google.gson.Gson;
import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;
import com.inventory.backend.repositories.ProductCategoryRepository;
import com.inventory.backend.repositories.ProductRepository;
import com.inventory.backend.security.SecurityConfiguration;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductCategoryRepository categoryRepository;

    @MockBean
    private ProductRepository productRepository;


    @Test
    void listCategories() throws Exception {
        Mockito.when(categoryRepository.findAll()).
                thenReturn(Stream.of(new ProductCategory(), new ProductCategory(), new ProductCategory()).collect(Collectors.toList()));
        this.mvc.perform(get("/categories"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)));
    }

    @Test
    void categoryById() throws Exception {

        Mockito.when(categoryRepository.findById(1L)).
                thenReturn(Optional.of(new ProductCategory("category01")));

        this.mvc.perform(get("/categories/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("category01")));
    }

    @Test
    void newCategory() throws Exception {

        ProductCategory productCategory = new ProductCategory("category01");
        Mockito.when(categoryRepository.save(productCategory)).
                thenReturn(productCategory);

        this.mvc.perform(post("/categories")
                .content(new Gson().toJson(productCategory))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.created_at").exists())
                .andExpect(jsonPath("$.updated_at").exists());
    }

    @Test
    void updateCategory() throws Exception {

        ProductCategory productCategory = new ProductCategory("category01");
        productCategory.setId(1L);

        Mockito.when(categoryRepository.findById(1L)).
                thenReturn(Optional.of(productCategory));
        Mockito.when(categoryRepository.save(productCategory)).
                thenReturn(productCategory);

        this.mvc.perform(post("/categories")
                .content(new Gson().toJson(productCategory))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.created_at").exists())
                .andExpect(jsonPath("$.updated_at").exists());

        productCategory.setName("category_updated");

        this.mvc.perform(put("/categories/1")
                .content(new Gson().toJson(productCategory))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.name", is(productCategory.getName())))
                .andExpect(jsonPath("$.updated_at").exists());

    }

    @Test
    void deleteProductCategory() throws Exception {
        doNothing().when(categoryRepository).deleteById(1L);
        this.mvc.perform(delete("/categories/1")).andExpect(status().isOk());
    }

    @Test
    void listProducts() throws Exception {

        Mockito.when(productRepository.findAll()).
                thenReturn(Stream.of(new Product(),
                        new Product(),
                        new Product()).collect(Collectors.toList()));
        this.mvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)));
    }

    @Test
    void productById() throws Exception {
        ProductCategory productCategory = new ProductCategory("category01");
        productCategory.setId(1L);

        Mockito.when(productRepository.findById(1L)).
                thenReturn(Optional.of(new Product(productCategory, "product01")));

        this.mvc.perform(get("/products/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("product01")));
    }

    @Test
    void newProduct() throws Exception {
        ProductCategory productCategory = new ProductCategory("category01");
        productCategory.setId(1L);
        Product product = new Product(productCategory, "product01");
        product.setId(1L);

        Mockito.when(categoryRepository.findById(1L)).
                thenReturn(Optional.of(productCategory));

        Mockito.when(productRepository.save(product)).thenReturn(product);

        this.mvc.perform(post("/products")
                .content(new Gson().toJson(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.name",is("product01")))
                .andExpect(jsonPath("$.created_at").exists())
                .andExpect(jsonPath("$.updated_at").exists());
    }

    @Test
    void listProductsByCategory() throws Exception {

        ProductCategory productCategory = new ProductCategory("category01");
        productCategory.setId(1L);

        Mockito.when(categoryRepository.findById(1L)).
                thenReturn(Optional.of(productCategory));

        Mockito.when(productRepository.findAllByCategory(productCategory)).
                thenReturn(Stream.of(new Product(productCategory,"product01"),
                                     new Product(productCategory,"product02"),
                                     new Product(productCategory,"product03")).collect(Collectors.toList()));

        this.mvc.perform(get("/categories/1/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)));
    }

    @Test
    void updateProduct() throws Exception {
        ProductCategory productCategory = new ProductCategory("category01");
        productCategory.setId(1L);
        Product product = new Product(productCategory, "product01");
        product.setId(1L);

        Mockito.when(categoryRepository.findById(1L)).
                thenReturn(Optional.of(productCategory));

        Mockito.when(productRepository.findById(1L)).
                thenReturn(Optional.of(product));

        Mockito.when(productRepository.save(product)).thenReturn(product);

        this.mvc.perform(post("/products")
                .content(new Gson().toJson(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.created_at").exists())
                .andExpect(jsonPath("$.updated_at").exists());

        product.setName("product_updated");
        product.setQuantity(12);
        product.setLocation("R3C5");


        this.mvc.perform(put("/products/1")
                .content(new Gson().toJson(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.name", is(product.getName())))
                .andExpect(jsonPath("$.quantity", is(product.getQuantity())))
                .andExpect(jsonPath("$.location", is(product.getLocation())))
                .andExpect(jsonPath("$.updated_at").exists());

    }

    @Test
    void deleteProduct() throws Exception {
        doNothing().when(productRepository).deleteById(1L);
        this.mvc.perform(delete("/products/1")).andExpect(status().isOk());
    }
}