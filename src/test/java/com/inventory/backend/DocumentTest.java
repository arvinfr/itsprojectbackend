package com.inventory.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import com.inventory.backend.models.Document;
import com.inventory.backend.models.DocumentType;
import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;
import com.inventory.backend.repositories.DocumentRepository;
import com.inventory.backend.repositories.ProductCategoryRepository;
import com.inventory.backend.repositories.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DocumentTest {

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    @Autowired
    private ProductRepository productRepository;

    private Set<Product> products;

    @BeforeEach
    void init() {
        documentRepository.deleteAll();
        productRepository.deleteAll();
        productCategoryRepository.deleteAll();
        products = new HashSet<>();
        ProductCategory category01 = new ProductCategory("categoryTest01");
        ProductCategory category02 = new ProductCategory("categoryTest02");
        Product product01 = new Product(category01, "productTest01");
        Product product02 = new Product(category01, "productTest02");
        Product product03 = new Product(category02, "productTest03");
        products.add(product01);
        products.add(product02);
        products.add(product03);
        productCategoryRepository.save(category01);
        productCategoryRepository.save(category02);
        productRepository.save(product01);
        productRepository.save(product02);
        productRepository.save(product03);
    }

    @Test
    void saveDocument() {
        Document document = new Document("Lorem Ipsum", products, DocumentType.ImportDocument);
        documentRepository.save(document);
        assertEquals(document.toString(), documentRepository.findById(document.getId()).get().toString());
    }

    @Test
    void addProduct(){
        Document document = new Document("Lorem Ipsum", products, DocumentType.ImportDocument);
        Product product04 = new Product(products.stream().iterator().next().getCategory(), "productTest04");
        document.addProduct(product04);
        documentRepository.save(document);
        assertEquals(4, documentRepository.findById(document.getId()).get().getProducts().size());
    }

    void removeProduct(){

    }

    
}
