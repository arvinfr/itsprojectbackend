package com.inventory.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.inventory.backend.models.Product;
import com.inventory.backend.models.ProductCategory;
import com.inventory.backend.repositories.ProductCategoryRepository;
import com.inventory.backend.repositories.ProductRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
class ProductTests {
    
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductCategoryRepository categoryRepository;
    
    @BeforeEach
    void init() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
    }

    @Test
    void saveCategory(){
        ProductCategory category = new ProductCategory("category1");
        categoryRepository.save(category);
        ProductCategory retrieved_category = categoryRepository.findById(category.getId()).get();
        assertEquals(category.toString(), retrieved_category.toString());
    }
    @Test
    void saveProduct() {
        ProductCategory category = new ProductCategory("category1");
        categoryRepository.save(category);
        Product product = new Product(category,"product1");
        productRepository.save(product);
        Product retrieved_Product = productRepository.findById(product.getId()).get();
        assertEquals(product.toString(), retrieved_Product.toString());
    }

    @Test 
    void findProductFromProductName(){
        ProductCategory category = new ProductCategory("category1");
        categoryRepository.save(category);
        Product product1 = new Product(category,"product1");
        Product product2 = new Product(category,"product2");
        productRepository.save(product1);
        productRepository.save(product2);
        Product rerieved_Product = productRepository.findProductByName(product1.getName());
        assertEquals(product1.toString(), rerieved_Product.toString());
    }

}
