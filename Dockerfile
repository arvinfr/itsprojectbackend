FROM openjdk:11-jre-slim
ADD build/libs/backend-PRODUCTION.0.2.jar backend-PRODUCTION.0.2.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "backend-PRODUCTION.0.2.jar"]